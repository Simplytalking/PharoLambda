#!/bin/sh
# Alpine shell script to deploy a Lambda Function

set -veu

if [[ ! -e deploy ]]; then
    echo -e "\e[91m\nMissing Deployment Assets!\n\e[0m"; exit 1;
fi

if [[ -z "$AWS_SECRET_ACCESS_KEY" ]]; then
    echo -e "\e[91m\nMissing AWS_ACCESS_KEY definitions in Pipeline Settings!\n\e[0m";
    exit 1;
fi

echo -e "\e[1m\nAssets available and AWS keys defined...\e[0m"
cd deploy
ls -lh

if [[ ! -z "$S3_BUCKET" ]]; then
    echo -e "\e[1m\nDeploying zip to S3...\e[0m";
    aws s3 cp $LAMBDA_NAME.zip s3://$S3_BUCKET;
fi

echo -e "\e[1m\nVerify Lambda functions status...\e[0m"
if (aws lambda get-function --function-name $LAMBDA_NAME); then
    echo -e "\e[1m\nUpdating existing Lambda function, $LAMBDA_NAME\e[0m";
    aws lambda update-function-code --function-name "$LAMBDA_NAME" --zip-file fileb://$LAMBDA_NAME.zip;
else
    echo -e "\e[1m\nDefining new Lambda function, $LAMBDA_NAME\e[0m";
    aws lambda create-function --function-name "$LAMBDA_NAME" --runtime "nodejs6.10" \
        --role $LAMBDA_ROLE --handler $LAMBDA_NAME.handler \
        --timeout 20 --zip-file fileb://$LAMBDA_NAME.zip --region $AWS_DEFAULT_REGION \
        --description "Pharo called from lambda";
fi

echo -e "\e[32m\nDeploy Complete!\n\e[0m";
echo ""
