"CmdLine script to load project"

| logger |

logger := FileStream stderr.
logger cr; nextPutAll: 'Starting Load Script...'.

"Create a stub TestCase (if needed) for packages not minimally defined"
Smalltalk at: #TestCase ifAbsent: [
    logger cr; nextPutAll: '>Creating Dummy TestCase'.
    Object subclass: #TestCase ].

logger cr; nextPutAll: '>Configure Load caches...'.
MCCacheRepository cacheDirectory: '../pharo-cache/' asFileReference.
MCGitHubRepository cacheDirectory: '../pharo-cache/' asFileReference.

logger cr; nextPutAll: '>Loading Projects...'; cr.

Metacello new
    baseline: 'Lambda';
    repository: 'filetree://../src';
    load.

logger cr; cr; nextPutAll: '<Loading Complete.'.

"If using a fat image - fix up some potential gotcha's"
Smalltalk at: #IceRepositoriesBrowser ifPresent: [ :i |
    logger cr; nextPutAll: '>Removing rogue ', i name.
    i allInstances do: [:b |
        logger nextPut: $+.
        b become: String new ]].

Smalltalk at: #OmSessionStore ifPresent: [ :i |
    logger cr; nextPutAll: '>Unregistering ', i name.
    SessionManager default unregisterClassNamed: i name ].


"Try and squeeze out some extra space by removing additional packages"
logger cr; nextPutAll: '>Removing Extra Packages'.
{ 'AWS-ElasticTranscoder'. 'AWS-CloudFront'.
'XML-Tests-Parser'. 'XML-Writer-Tests'.  'XML-Writer-Core' } do: [ :name |
    logger cr; nextPutAll: '+Removing: ', name.
	(MCPackage named: name) unload ].

logger cr; nextPutAll: '>Removing TestCases'.
(Smalltalk at: #TestCase) allSubclasses do: [ :c | c removeFromSystem. logger nextPut: $+  ].

3 timesRepeat: [
        Smalltalk garbageCollect.
        Smalltalk cleanOutUndeclared.
        Smalltalk fixObsoleteReferences].

logger cr; nextPutAll: 'Finished Load Script.'; cr; cr.