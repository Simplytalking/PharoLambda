private
bucketNamed: bucketNameString
	^(super bucketNamed: bucketNameString)
		service: self;
		yourself