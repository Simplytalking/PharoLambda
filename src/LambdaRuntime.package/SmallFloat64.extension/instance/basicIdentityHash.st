*LambdaRuntime
basicIdentityHash
	"Quick fix to workaround 64 bit image issue.
	see: https://pharo.fogbugz.com/f/cases/20120/Fuel-is-not-64bits-ready"
	
	^self identityHash